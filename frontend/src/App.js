import './App.css';
import Exerside from './views/Exerside';
import InputNewVocabulary from './views/InputNewVocabulary'
import ListVocabulary from './views/ListVocabulary';

function App() {
  return (
    <>
      <div className='App'>
        <div style={{width:'30%', flexDirection:'column'}}>
          <InputNewVocabulary />
          <Exerside />
        </div>
        <div style={{width:'70%', marginLeft:40, marginRight:40}}>
          <ListVocabulary />
        </div>
      </div>
    </>
  )
}

export default App;
