import { useState } from 'react';
import './style.scss'

const InputNewVocabulary = () => {
    const initialFormData = Object.freeze({
        eng: "",
        vni: ""
    });
    const [formData, setFormData] = useState(initialFormData)

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value.trim()
        });
    };

    const handleSubmit = () => {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formData)
        };

        fetch('http://192.168.1.9:5001/', requestOptions)
            .then(response => response.json())
            .then(data => console.log(data));

        setFormData(initialFormData)
    }

    return (
        <div id="add-new-vocabulary">
            <div className="conatiner">
                <p className="title">Create Vocabulary</p>
                <div className="input-container">
                    <input placeholder="English" name="eng" onChange={handleChange} value={formData.eng} />
                    <input placeholder="Vietnamese" name="vni" onChange={handleChange} value={formData.vni} />
                </div>
                <div className="btn-container">
                    <button
                        className="btn--submit"
                        onClick={() => handleSubmit()}
                    >Add new</button>
                </div>
            </div>
        </div>
    )
}

export default InputNewVocabulary
