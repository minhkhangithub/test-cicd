import { useEffect, useMemo, useState } from 'react'
import { usePagination, useTable } from 'react-table'
import './style.scss'

const ListVocabulary = () => {

    useEffect(() => {
        getData()
        setPageSize(12)
    }, [])

    const [data, setData] = useState([])

    const getData = () => {
        fetch("http://192.168.1.9:5001/")
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    const columns = useMemo(
        () => [
            {
                Header: 'English',
                accessor: 'eng',
            },
            {
                Header: 'Vietnamese',
                accessor: 'vni',
            },
            {
                Header: 'Status',
                accessor: 'col3',
            },
            {
                Header: 'Action',
                accessor: 'col4',
            },
        ],
        []
    )


    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        nextPage,
        previousPage,
        setPageSize
    } = useTable({ columns, data }, usePagination)

    return (
        <div id="list-vocabulary">
            <div className="conatiner" >
                <div className="title-container">
                    <p className="title">Vocabulary</p>
                </div>
                <div className="control-container">
                    <p>Show</p>

                </div>
                <div className="list-item-container">
                    <table {...getTableProps()}>
                        <thead>
                            {headerGroups.map(headerGroup => (
                                <tr {...headerGroup.getHeaderGroupProps()}  id="table-header">
                                    {headerGroup.headers.map(column => (
                                        <th {...column.getHeaderProps()}>
                                            {column.render('Header')}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        <tbody {...getTableBodyProps()}>
                            {page.map(row => {
                                prepareRow(row)
                                return (
                                    <tr {...row.getRowProps()}>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()} >
                                                    {cell.render('Cell')}
                                                </td>
                                            )
                                        })}
                                    </tr>)
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="btn-container">
                    <button className="btn" id="btn--random" onClick={()=> getData()} >Reload</button>
                    <button className="btn" id="btn--show" onClick={() => previousPage()}>Previous</button>
                    <button className="btn" id="btn--check" onClick={() => nextPage()}>Next</button>
                </div>

            </div>
        </div>
    )
}

export default ListVocabulary
