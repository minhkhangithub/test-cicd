import React, { useMemo, useState } from 'react';

import './style.scss'

const Exerside = () => {
    const [language, setLanguage] = useState(true)
  

    const handleClick = (status) => {
        setLanguage(status)
    }


    return (
        <div id="exerside">
            <div className="conatiner">
                <div className="title-container">
                    <p className="title">Exerside</p>
                    <div className="switch-btn">
                        <button id="btn--vn"
                            style={{ backgroundColor: language ? '#FFF' : '#7367f0', color: language ? '#5e5873' : '#FFFFFF' }}
                            onClick={() => handleClick(false)}>
                            ENG</button>
                        <button id="btn--eng"
                            style={{ backgroundColor: language ? '#7367f0' : '#FFF', color: language ? '#FFFFFF' : '#7367f0' }}
                            onClick={() => handleClick(true)}>
                            VN</button>
                    </div>
                </div>
                <form className="input-container" >
                    <input placeholder="English" name="eng" disabled={language ? "disabled" : ""} />
                    <input placeholder="Vietnamese" name="vni" disabled={language ? "" : "disabled"} />
                </form>
                <div className="btn-container">
                    <button
                        className="btn" id="btn--random"
                    >Random</button>
                    <button className="btn" id="btn--show">Forget</button>
                    <button className="btn" id="btn--check">Check</button>
                </div>
            </div>
        </div>
    )
}

export default Exerside