const mysql = require('mysql2')

var mysqlHost = process.env.MYSQL_HOST || '192.168.1.9';
var mysqlPort = process.env.MYSQL_PORT || '3306';
var mysqlUser = process.env.MYSQL_USER || 'lucifer';
var mysqlPass = process.env.MYSQL_PASS || '987123';
var mysqlDB = process.env.MYSQL_DB || 'testdb';

var connectionOptions = {
    host: mysqlHost,
    port: mysqlPort,
    user: mysqlUser,
    password: mysqlPass,
    database: mysqlDB
};

const conc = mysql.createConnection(connectionOptions)
conc.connect()

let db = {}

db.getData = () => {
    return new Promise((reslove, reject) => {
        conc.query(`SELECT * FROM vocabulary`, (err, results) => {
            if (err) return reject(err)
            else return reslove(results)
        })
    })
}

db.insert = (data) => {
    return new Promise((reslove, reject) => {
        conc.query(`INSERT INTO vocabulary(eng,vni) VALUES("${data.eng}","${data.vni}")`, (err, results) => {
            if (err) return reject(err)
            else return reslove(results)
        })
    })
}

module.exports = db
