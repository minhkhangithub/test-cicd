const express = require('express')
const app = express()
const PORT = 4000
const db = require('./db')
const cors = require('cors')
app.use(express.urlencoded())
app.use(express.json())
app.use(cors({
    origin: "*",
    methods: ["GET", "POST", "DELETE"]
}))

app.get('/', (req, res) => {
    db.getData().then(result => {
        res.send(result)
    })
})

app.post('/', (req, res) => {
    console.log(req.body)
    db.insert(req.body).then(result => {
        res.status(200).send(result)
    })
})

app.listen(PORT, () => { console.log("Server start.") })
